/**
 * Define a request router
 */

// Dependencies
const handlers = require('./handlers');

const router = {
    '': handlers.index,
    'login': handlers.accountLogin,
    'signup': handlers.accountCreate,
    'menu': handlers.menuDisplay,
    'shopping-cart': handlers.shoppingCartPreview,
    'order': handlers.orderCreate,
    'order-created': handlers.orderCreated,
    'public': handlers.public,
    'api/users': handlers.users,
    'api/tokens': handlers.tokens,
    'api/menu': handlers.menu,
    'api/shopping-carts': handlers.shoppingCarts,
    'api/orders': handlers.orders
};

module.exports = router;